package de.starmixcraft.wdls;

import org.bukkit.plugin.java.JavaPlugin;

import de.starmixcraft.wdls.netty.EventLoopManager;
/*
 * Das system arbeitet systemmatisch requetst ab, dadurch wird der server nicht sehr belastet,
 * nachteil es kann sein das das packet erst im nächsten tick geprocesst wird dies hat allerdings keinerlei auswirklungen,
 * man merkt es nicht mal, ich habe die packets jetzt nicht modifizirt weil das mir zu viel arbeit ist,
 * aber mit diesem EventLoop system sollte der wdls viel weniger leistung brauchen da er die arbeit aufteilt
 * 
 * CopyRight Starmixcraft
 * Uneingeschrenktes und unwiederrufliches nutzungsrecht MyPlayPlanet.net und alles damit verbundenen componenten
 * 
 */
public class Main extends JavaPlugin{
	
	private static Main instance;
	
	public static Main getInstance() {
		return instance;
	}
	
	@Override
	public void onEnable() {
		instance = this;
		new EventLoopManager(32);
		
	}

}
