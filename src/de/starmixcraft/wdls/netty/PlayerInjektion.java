package de.starmixcraft.wdls.netty;

import java.lang.reflect.Field;
import java.util.List;
import java.util.function.Consumer;

import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import net.minecraft.server.v1_12_R1.Block;
import net.minecraft.server.v1_12_R1.Material;
import net.minecraft.server.v1_12_R1.MaterialMapColor;
import net.minecraft.server.v1_12_R1.Packet;
import net.minecraft.server.v1_12_R1.PacketPlayOutBlockChange;
import net.minecraft.server.v1_12_R1.PacketPlayOutMapChunk;
import net.minecraft.server.v1_12_R1.PacketPlayOutMultiBlockChange;

public class PlayerInjektion {
	
	private Player player;
	
	private Channel channel;
	
	private Block block;
	
	public PlayerInjektion(Player player) {
		block = new Block(Material.STONE, MaterialMapColor.I);
		this.player = player;
		this.channel = ((CraftPlayer)this.player).getHandle().playerConnection.networkManager.channel;
		injekt();
	}
	public PlayerInjektion(Channel channel) {
		block = new Block(Material.STONE, MaterialMapColor.I);
		this.channel = channel;
		injekt();
	}
	
	
	
		private void injekt() {
			channel.pipeline().addAfter("decoder", "PacketModifer", new MessageToMessageDecoder<Packet<?>>() {

				@Override
				protected void decode(ChannelHandlerContext handler, Packet<?> packet, List<Object> list) throws Exception {
					
					modifie(packet, new Consumer<Packet<?>>() {

						@Override
						public void accept(Packet<?> t) {
							list.add(t);
							
						}
					});
					
				}
			});
		}
		
		
		
		private void modifie(Packet<?> packet, Consumer<Packet<?>> consumer){
			EventLoopManager.getInstance().addRequest(new ProcessRequests() {
				
				@Override
				public void run() {
					if(packet instanceof PacketPlayOutBlockChange) {
						
						PacketPlayOutBlockChange blockChange = (PacketPlayOutBlockChange) packet;
					set(blockChange, "block", PlayerInjektion.this.block);
					consumer.accept(blockChange);			
					}else if(packet instanceof PacketPlayOutMultiBlockChange) {
						//auch mit Mulitblock change machen was ich oben gemacht habe
					}else if(packet instanceof PacketPlayOutMapChunk) {
						//auch mir dem MapChunk packet machen was ich oben gemacht habe
					}
					consumer.accept(packet);
					
				}
			});
		}
		
		
		
		
		private void set(Object instance, String name, Object value) {
			set(instance.getClass(), instance, name, value);
		}
		private void set(Class<?> clazz, Object instance, String name, Object value) {
			try {
				Field field = clazz.getDeclaredField(name);
				field.setAccessible(true);
				field.set(instance, value);
			} catch (Exception e) {
				System.out.println(e);
			}
		}
		private Object get(Object instance, String name) {
			return get(instance.getClass(), instance, name);
		}
		private Object get(Class<?> clazz, Object instance, String name) {
			try {
				Field field = clazz.getDeclaredField(name);
				field.setAccessible(true);
				return field.get(instance);
			} catch (Exception e) {
				System.out.println(e);
			}
			return null;
		}

}
