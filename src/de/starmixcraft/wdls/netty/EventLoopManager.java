package de.starmixcraft.wdls.netty;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import de.starmixcraft.wdls.Main;


public class EventLoopManager {
	
	private static EventLoopManager instance;
	private Thread[] threads;
	
	private ArrayList<ProcessRequests> processRequests;
	
	public static EventLoopManager getInstance() {
		return instance;
	}
	
	
	public ProcessRequests getNextRequest() {
		return processRequests.remove(0);
	}
	
	
	public void addRequest(ProcessRequests requests) {
		processRequests.add(requests);
	}
	
	
	
	private void TryToProcess() {
		for(Thread thread : threads)
		if(!thread.isAlive() && processRequests.size() > 0) {
			thread.run();
		}
	}
	
	public EventLoopManager(int threads) {
		instance = this;
		processRequests = new ArrayList<>();
		this.threads = new Thread[threads];
		for (int i = 0; i < this.threads.length; i++) {
			this.threads[i] = new Thread(new ProcessRunner());
		}
		
		new BukkitRunnable() {
			
			@Override
			public void run() {
				//execute the EventLoop ons per tick
				TryToProcess();
				
			}
		}.runTaskTimer(Main.getInstance(), 1, 1);
		
		System.out.println("EventLoop online");
	}
	
	
	

}
